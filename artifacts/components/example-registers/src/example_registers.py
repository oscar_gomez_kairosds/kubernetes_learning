#Description
"""
Searches for registers using earthquake
codes as inputs, returns register information
"""


#Libraries
##Packages
from argparse import ArgumentParser
from unittest import registerResult
import requests


#Argparse
parser = ArgumentParser()
parser.add_argument("--input-path", type=str)
parser.add_argument("--output-path", type=str)
args = parser.parse_args()


#Main function
def extract_registers(earthquake_code):
    ##Documentation
    """
    Description
    -----------
    Queries ICGC's earthquake database for gse documentation on
    an earthquake using its code. This documentation includes 
    phase arrival times, trace amplitudes and magnitudes of each
    station that recorded the earthquake.

    A table of stations registers is selected among other general
    data. If no register table is found a None value will be and 
    the earthquake will be discarted. If the table is found it will
    be parsed and returned.
    
    Parameters
    ----------
    earthquake_code: str
        An ICGC earthquake code, a 5 digit number
    
    Returns
    -------
    register_table: list | None
        A list of tuples containing each line of the register table, on gse
        files P wave and S wave registers are separated and have different
        data points. These function will return the raw separated registers.
        If the gse file does not contain a station register table
        None is returned
    """
    
    ##Querying ICGC database
    response = requests.get('https://sismocat.icgc.cat/sisweb2/siswebclient_gse_external.php?seccio=gse&codi=' + earthquake_code)

    ##Selecting table
    has_registers = False
    register_table = str(response.content).split('\\r\\n')
    for i,line in enumerate(register_table):
        if line.startswith('Sta'):
            register_index = i + 1
            has_registers = True
    if has_registers == False:
        register_table = None
        return register_table
    register_table = register_table[register_index:-2]

    ##Parsing register table
    register_table = list(map(
        lambda line: tuple(filter(
            lambda w: w != '',
            line.split(' ')
        )),
        register_table
    ))
    return register_table


#Execution
with open(args.input_path, 'r') as input_file:
    with open(args.output_path, 'w') as output_file:
        for code in input_file.readlines():
            register_table = extract_registers(code)
            for register in register_table:
                _ = output_file.write(str(register)[1:-1].replace("'", '') + "\n")