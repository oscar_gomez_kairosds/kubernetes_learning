#Description
"""
Reads a csv with earthquake metadata and extracts its earthquake codes
"""

#Libraries
##Standard
from argparse import ArgumentParser
from os import chdir, path
from csv import reader

#Argparse
parser = ArgumentParser()
parser.add_argument("--output-path", type=str)
args = parser.parse_args()

#Main function
def parse_earthquake_metadata(input_file, output_file):
    earthquake_medata_rows = reader(input_file)
    next(earthquake_medata_rows)
    for emr in earthquake_medata_rows:
        e_code = emr[0]
        _ = output_file.write(e_code + "\n")

#Execution
with open('/pipelines/component/src/data/earthquake_metadata.csv', 'r') as input_file:
    with open(args.output_path, 'w') as output_file:
        parse_earthquake_metadata(input_file, output_file)