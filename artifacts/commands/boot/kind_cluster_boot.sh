#!/bin/bash

#Asignamos un nombre predeterminado al cluster
my_cluster_name="my-cluster"
assign_my_cluster=1

#Comprobamos las flags por si hemos aportado un nombre
while :; do
	case $1 in 
		-n|--name) my_cluster_name=$2;;
		-a|--assign) assign_my_cluster=$2;;
		*) break
	esac
	shift
done

#Comprobamos que no hay clusters activos el mismo nombre
for cluster_name in $(kind get clusters)
do
	if [ "$my_cluster_name" == "$cluster_name" ]; then
		echo ${my_cluster_name} is already booted in kind
		exit
	fi
done

#Creamos nuestro cluster y asignamos su contexto a kubectl
kind create cluster --name=${my_cluster_name}
if [ "$assign_my_cluster" == 1 ]; then
	kubectl cluster-info --context kind-${my_cluster_name}
fi

#Guardamos su kubeconfig en la carpeta de kubeconfigs de kind
#kubeconfig_path="../../kubeconfigs/kind/kind_kubeconfig_${my_cluster_name}"
#touch $kubeconfig_path
#kind get kubeconfig --name $my_cluster_name>$kubeconfig_path
#readlink -f $kubeconfig_path
