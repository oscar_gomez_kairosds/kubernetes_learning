#Instalar kubectl en Linux
##Obtenemos la última versión de kubectl
sudo apt-get install curl
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"

#Habilitamos premisos de ejecución para el binario kubectl
chmod +x ./kubectl

#Movemos el binario a una carpeta dentro incluida en PATH
sudo mv ./kubectl /usr/local/bin/kubectl

#Comprobamos que tenemos la versión indicada
echo $(kubectl version --client=true)
