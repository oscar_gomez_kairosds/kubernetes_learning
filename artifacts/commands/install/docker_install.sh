#Desinstalamos versiones antiguas
sudo apt-get remove docker docker-engine docker.io containerd runc


#Instalar Docker mediante repositorio
##Instalando dependencias de repositorio
sudo apt-get update
apt-get install \
	ca-certificates \
	curl \
	gnupg \
	lsb-release

##Descargarmos añadiendo la clave GPG oficial de Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg1

##Creamos un repositorio estable
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


#Instalación de Docker Engine
##Instalar Docker Engine
sudo apt-get update
sudo apt-get install -y docker.io

##Permitimos a nuestro usuario usar docker
sudo usermod -aG docker $1

##Comprobar el funcionmiento de Docker
sudo docker run hello-world
