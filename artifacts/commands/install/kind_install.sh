#Actualizaciones de sistema
sudo apt update && apt upgrade -y

#Descarga, borrado de versiones antiguas, untar
wget https://go.dev/dl/go1.17.6.linux-amd64.tar.gz
sudo rm -rf /usr/local/go
sudo tar -C /usr/local -xzf go1.17.6.linux-amd64.tar.gz

#Config GO
echo "export PATH=$PATH:/usr/local/go/bin" >> $HOME/.profile
echo "export GOROOT=/usr/local/go" >> $HOME/.profile
source $HOME/.profile

#Instalar kind
GO111MODULE="on" go install sigs.k8s.io/kind@v0.11.1

#Config kind
echo "export PATH=$PATH:$HOME/go_projects/bin" >> ~/.bashrc

#Limpieza
rm go*

#ejecutar source ~/.bashrc después de este script si se quiere
# continuar con la misma consola
