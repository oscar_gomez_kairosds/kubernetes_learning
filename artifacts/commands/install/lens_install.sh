current_app_image=Lens-5.3.3-latest.20211223.1.x86_64.AppImage
wget https://api.k8slens.dev/binaries/$current_app_image
cd ../boot
mkdir lens
mv ../install/$current_app_image ./lens
echo "./lens/$current_app_image" > ./lens_boot.sh
chmod +x ./lens_boot.sh
chmod +x ./lens/$current_app_image
