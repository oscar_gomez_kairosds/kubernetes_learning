#Extracting arguments
##Positional
image_name=$1
dirname=$2
local_tag="latest"
remote_tag=$local_tag
shift 2

##Optional
while :; do
    echo $1
	case $1 in 
		--local-tag) local_tag=$2 && remote_tag=$local_tag;;
        --remote-tag) remote_tag=$2;;
		*) break
	esac
	shift 2
done

##Environment
docker_repo="ognkairos20211011"


#Building image and pushing to remote (dirname must have a dockerfile)
cd $dirname
local_name="${image_name}:${local_tag}"
remote_name="${docker_repo}/kubeflow-learning-${image_name}:${remote_tag}"
docker login
docker build -t "${local_name}" .
docker tag "${local_name}" "${remote_name}"
docker push "${remote_name}"
docker rmi $remote_name