#!/bin/bash

#Cerramos el cluster
kind delete cluster --name $1

#Borramos su kubeconfig
cd ../../kubeconfigs/kind
rm $(ls | grep _$1)
