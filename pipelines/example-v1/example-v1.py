from pyexpat.errors import codes
import kfp

get_codes = kfp.components.load_component_from_file(
    "/home/ubuntu/Desktop/learning/kubernetes/artifacts/components/example-csv/example-csv-spec.yaml"
)

get_registers = kfp.components.load_component_from_file(
    "/home/ubuntu/Desktop/learning/kubernetes/artifacts/components/example-registers/example-registers-spec.yaml"
)

@kfp.dsl.pipeline(
    name="example-pipeline",
    description="Identify earthquake registers",
    pipeline_root="gs://example-outputs-bucket"
)
def example_pipeline_v1():
    codes = get_codes()
    get_registers(codes.output['Earthquake Codes'])


kfp.compiler.Compiler().compile(
    pipeline_func = example_pipeline_v1,
    package_path = "example-v1.yaml"
)